import React, {Component} from 'react';
import './GitHub.css';

import axios from "axios";
import SearchForm from "../../components/searchForm/searchForm";
import Loader from "../../components/UI/Loader/Loader";

class GitHub extends Component {

    state = {
        username: "",
        loader: false,
        user: "",
        errorMsg: null
    }

    inputHandler = (event) => {
        this.setState({username: event.target.value});
    }

    searchHandler = (e) => {
        e.preventDefault();
        this.setState({loader: true});
        axios.get('https://api.github.com/users/' + this.state.username)
            .then((res) => {
                this.setState({loader: false, user: res.data, errorMsg: null});
            }).catch((err) => {
                this.setState({errorMsg: err.response.data.message});
        })
    }

    render() {
        return (
            <div>
                <SearchForm inputHandler={this.inputHandler} search={this.searchHandler} />
                {/* Loader */}
                {this.state.loader && this.state.errorMsg == null ? <Loader/> : null}
                {this.state.errorMsg != null ? <h1 style={{textAlign: "center"}}>{this.state.errorMsg}</h1> : null}
                {this.state.loader || this.state.user === "" ? null : (
                    <div className="container">
                        <div className='col-md-12'>
                            <div className="avatar float-left" style={{backgroundImage: "url(" + this.state.user.avatar_url + ")"}}></div>
                            <div className='basicInfo'>
                                <h1>{this.state.user.name} ({this.state.user.login})</h1>
                                {this.state.user.followers != null && this.state.user.following != null ? (
                                    <ul>
                                        <li>Followers: {this.state.user.followers}</li>
                                        <li>Following: {this.state.user.following}</li>
                                    </ul>
                                ) : null }
                                <button>
                                    <a href={this.state.user.html_url} target='_blank'>View Profile</a>
                                </button>
                            </div>
                        </div>
                    </div>
                )}
            </div>
        )
    }
}

export default GitHub;