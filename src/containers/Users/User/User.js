import React, {Component} from 'react';
import axios from 'axios';
import './User.css';

class User extends Component {

    constructor() {
        super();
        this.state = {
            user: ''
        }
    }

    componentDidMount () {
        const userId = this.props.match.params.id;
        axios.get('https://jsonplaceholder.typicode.com/users/' + userId)
            .then((res) => {
                this.setState({user: res.data});
            })
    }

    render() {
        const userData = (
            this.state.user === '' ? null : (
                <div className="container">
                    <div className="wrapperUser">
                        <h1>{this.state.user.name}</h1>
                        <h3>{this.state.user.username}</h3>
                        <ul>
                            <li>Phone: {this.state.user.phone}</li>
                            <li>Website: {this.state.user.website}</li>
                            <li>City: {this.state.user.address.city}</li>
                            <li>Street: {this.state.user.address.street}</li>
                        </ul>
                    </div>
                </div>
            )
        )
        return (
            <div>
                {userData}
            </div>
        )
    }
}

export default User;