import React, {Component} from 'react';
import axios from 'axios';
import './Users.css';

class Users extends Component {

    state = {
        users: []
    }

    componentWillMount() {
        axios.get('https://jsonplaceholder.typicode.com/users')
            .then((res) => {
                this.setState({users: res.data});
                console.log(res.data)
            })
    }

    getUserHandler = (id) => {
        this.props.history.push('user/' + id);
    }

    render() {
        const usersList = (
            this.state.users.map((u, index) => {
                return (
                    <div className="col-md-4 marginBottom" key={index}>
                        <div className="userBlock" onClick={() => this.getUserHandler(u.id)}>
                            <div className="content">
                                <h4>{u.name}</h4>
                                <p>({u.username})</p>
                            </div>
                        </div>
                    </div>
                )
            })
        )

        return (
            <div className='container'>
                <div className="wrapper">
                    <div className="row">
                        {usersList}
                    </div>
                </div>
            </div>
        )
    }
}

export default Users;