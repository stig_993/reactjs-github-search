import React, {Component} from 'react';
import {Route, Switch} from 'react-router-dom';
import Navigation from '../../components/Navigation/Navigation';
import Git from '../GitHub/GitHub';
import Users from '../Users/Users';
import User from '../Users/User/User';
import './App.css';

class App extends Component {
    render() {
        return (
            <div className="App">
                <Navigation />
                <Switch>
                    <Route path="/" exact component={Git}></Route>
                    <Route path="/users" exact component={Users}></Route>
                    <Route path="/user/:id" exact component={User}></Route>
                </Switch>
            </div>
        );
    }
}

export default App;
