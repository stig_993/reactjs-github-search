import React from 'react';

import "./Navigation.css";
import logo from "../../assets/img/logo.png";
import {NavLink} from 'react-router-dom';

const navigation = () => {
    return (
        <div className='navigation'>
            <div className="container">
                <div className="logoDiv float-left">
                    <img src={logo} alt=""/>
                </div>
                <div className='links float-right'>
                    <NavLink to="/" exact activeClassName='active'>Search</NavLink>
                    <NavLink to="/Users" exact activeClassName='active'>Users</NavLink>
                </div>
            </div>
        </div>
    )
}

export default navigation;