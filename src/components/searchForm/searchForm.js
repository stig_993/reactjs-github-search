import React from 'react';
import './searchForm.css'

const SearchForm = (props) => {
    return (
        <div>
            <form onSubmit={props.search}>
                <input type="text" name="username" onChange={props.inputHandler} placeholder="Search"/>
                <button>Search</button>
            </form>
        </div>
    )
}

export default SearchForm;